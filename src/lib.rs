#[no_mangle]
pub fn version_major() -> i32 { 0 }
#[no_mangle]
pub fn version_minor() -> i32 { 1 }
#[no_mangle]
pub fn version_patch() -> i32 { 0 }
